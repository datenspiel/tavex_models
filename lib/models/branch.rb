# -*- encoding : utf-8 -*-
class Branch < Datenspiel::Base

  def is_business?
    return self.identifier.eql?("business")
  end
  
  def is_private?
    return self.identifier.eql?("private")
  end
  
end
