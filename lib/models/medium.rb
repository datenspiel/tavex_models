# -*- encoding : utf-8 -*-
unless defined?(Datenspiel)
  require File.expand_path(File.join(File.dirname(__FILE__), 'base.rb'))
end

class Medium < Datenspiel::Base

  with_options :dependent => :destroy do |assoc|
    has_many :categories, :class_name => "Category", :foreign_key => "category_id"
    has_many :campaigns, :class_name => "Campaign", :foreign_key => "medium_id"
  end

  def is_gas?
    return self.identifier.downcase.eql?("gas")
  end
  
  def is_power?
    return (self.identifier.downcase.eql?("strom") or self.identifier.downcase.eql?("wärme"))
  end

  def json_attributes
    attrs = {
        :identifier => self.identifier,
        :id => self.id,
        :is_power => self.is_power?,
        :is_gas => self.is_gas?
      }
  end

  def as_json(opt)
    {self.class.name.underscore.to_sym => json_attributes}
  end
end
