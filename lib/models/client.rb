# -*- encoding : utf-8 -*-
# @author Daniel Schmidt, Datenspiel GmbH 2010
# rails model for client
class Client < Datenspiel::Base

  before_save :set_default_exchange_ratio
  before_save :divider_for_calculation
  
  default_scope :order => "identifier"

  acts_as_product_for_client_enhanced 
  has_as_client_validations
  acts_as_client_json
  has_branch_methods
  has_product_methods

  def logo_url
    self.logo.url(:small).split("?").first unless self.logo.nil?
  end
  
  def logo_url=(url);end

  def set_default_exchange_ratio
    if (self.exchange_ratio==nil)
      self.exchange_ratio=11.0
    end
  end

  def divider_for_calculation
    self.monthly_divider.nil? ? 12 : self.monthly_divider
  end
end
