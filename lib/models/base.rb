# -*- encoding : utf-8 -*-
module Datenspiel

  class Base < ActiveRecord::Base
    self.abstract_class = true

    def attached_errors
      errors.full_messages.join("\n")
    end

    def failure(options={})
      include_data = options.fetch(:include_errors, false)
      hsh = {:success => false, :errors => self.attached_errors}
      if include_data
        hsh[:data] = self.errors
      end
      hsh
    end

    def success(*args)
      options = args.extract_options!
      result  = {:success => true}

      if options.has_key?(:model)
        if options[:model]
          result[:model] = self.as_json
        end
      end
      return result
    end

    def creator_name
      name = ""
      if self.respond_to?(:creator)
       name = self.creator.login unless self.creator.nil?
      end
      return name
    end

    def for_association_json
      {self.class.name.underscore.to_sym => self.attributes}
    end

    # INTERNAL: Exports hash with given attributes.
    #
    # attrs - The attributes to export.
    def export_attrs(*attrs)
      tmp = {}
      attrs.each do |attr|
        t = {}
        t[attrs.is_a?(Symbol) ? attr: attr.to_sym] = self.send(attr)
        tmp.merge!(t)
      end
      {self.class.name.underscore.to_sym => tmp}
    end
    alias_method :to_attrs, :export_attrs

  end


end
