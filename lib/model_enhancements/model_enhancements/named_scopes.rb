# -*- encoding : utf-8 -*-
module Scopes

  module NamedScopes

    def user_scopes
      # returns all users except the master user
      self.scope :users_without_role, lambda{|role|
        where("role_name != ?", role)
      }
      #self.named_scope :users_without_role, lambda { |role|
      #  { :conditions => ["role_name != ?", role] }
      #} #same as User.find(:all, :conditions =>["role_name!=?", role])

      self.scope :notifiable_users, where(:notify_by_mail => true)
      #self.named_scope :notifiable_users, :conditions => {:notify_by_mail => true}

      self.scope :all_by_name, order("name,surename ASC")
      #self.named_scope :all_by_name, :order => "name,surename ASC"

      # get a list of administrators and moderators
      self.scope :administration_users, where("role_name = ? or role_name = ?", "administrator", "moderator")
      #self.named_scope :administration_users, :conditions => ["role_name = ? or role_name = ?", "administrator", "moderator"]

      self.scope :available_for_search_with_email_like, lambda{ |mail|
        where("email like ? and exclude_from_search = false", "%"+mail+"%")
      }
      #self.named_scope :available_for_search_with_email_like, lambda{ |mail|
      #  {:conditions => ["email like ? and exclude_from_search = false", mail]}
      #}

      self.scope :available_for_search_with_name_or_surename_like, lambda{|name|
        where("(name like ? or surename like ?) and exclude_from_search = false","%"+name+"%","%"+name+"%")
      }
      #self.named_scope :available_for_search_with_name_or_surename_like, lambda{|name|
      #  {:conditions => ["(name like ? or surename like ?) and exclude_from_search = false",name,name ]}
      #}
    end

    def product_scope
      
      self.scope :by_product, lambda{ |product_id|
        where(:product_id => product_id)
      }
      #self.named_scope :by_product, lambda {|product_id|
      #  {:conditions => {:product_id => product_id}}
      #}

    end

    def uptake_rate_scope
      self.scope :filter_uptake_rates, lambda{|consumption_value|
        where("kilowatt_hour_from<=? and kilowatt_hour_to>=?", consumption_value,consumption_value)
      }
      #self.named_scope :filter_uptake_rates, lambda{|consumption_value|
      #  {:conditions =>["kilowatt_hour_from<=? and kilowatt_hour_to>=?", consumption_value,consumption_value]}
      #}
      self.scope :find_by_kilowatt_hour_from, lambda {|consumption_value|
        where("kilowatt_hour_from<=?", consumption_value)
      }
      #self.named_scope :find_by_kilowatt_hour_from, lambda {|consumption_value|
      #  {:conditions =>["kilowatt_hour_from<=?", consumption_value]}
      #}
    end

    def identifier_scopes
      self.scope :like_by_name, lambda{|like|
        where("identifier like ? ","%" + like + "%")
      }
      #self.named_scope :like_by_name, lambda {|like|
      #  {:conditions => ["identifier like ? ","%" + like + "%"]}
      #}
    end

    def product_date_scope
      self.scope :by_new_to_date, lambda{|date|
        where("valid_thru_new_to <= ?", date).order("valid_thru_new_to ASC")
      }
      #self.named_scope :by_new_to_date, lambda {|date|
      #  {:conditions => ["valid_thru_new_to <= ?", date], :order => "valid_thru_new_to ASC"}
      #}
      
      self.scope :by_new_from_date, lambda{|date|
        where("valid_thru_new_from >= ?", date).order("valid_thru_new_from ASC")
      }
      #self.named_scope :by_new_from_date, lambda {|date|
      #  {:conditions => ["valid_thru_new_from >= ?", date], :order => "valid_thru_new_from ASC"}
      #}
      self.scope :by_within_new_dates, lambda{ |from,to|
        where("valid_thru_new_from >=? and valid_thru_new_to <= ?", from,to)
      }
      #self.named_scope :by_within_new_dates, lambda {|from,to|
      #  {:conditions => ["valid_thru_new_from >=? and valid_thru_new_to <= ?", from,to]}
      #}
      self.scope :by_existence_to_date, lambda {|to|
        where("valid_thru_existence_to <= ?", to).order("valid_thru_existence_to ASC")
      }
      #self.named_scope :by_existence_to_date, lambda {|to|
      #  {:conditions => ["valid_thru_existence_to <= ?", to], :order => "valid_thru_existence_to ASC"}
      #}
    end

    def product_type_filters
      self.scope :filter_existence_products, lambda{ |date|
        where("valid_thru_new_from<=? and valid_thru_existence_to>=?",date,date)
      }
      #self.named_scope :filter_existence_products, lambda{|date|
      #  {:conditions =>["valid_thru_new_from<=? and valid_thru_existence_to>=?", date,date]}
      #}
      
      self.scope :filter_new_products, lambda{ |date| 
        where("valid_thru_new_from<=? and valid_thru_new_to>=?", date,date)
      }
      #self.named_scope :filter_new_products, lambda{|date|
      #  {:conditions =>["valid_thru_new_from<=? and valid_thru_new_to>=?", date,date]}
      #}

    end

    def client_attached_scopes
      # all named scopes above with client id
      self.scope :client_like_by_name,lambda {|like,client|
        where("client_id = ? and identifier like ? ",client,"%" + like + "%")
      }
      #self.named_scope :client_like_by_name, lambda {|like,client|
      #  {:conditions => ["client_id = ? and identifier like ? ",client,"%" + like + "%"]}
      #}
      
      self.scope :client_by_new_to_date, lambda{ |date,client|
        where("client_id = ? and valid_thru_new_to <= ?",client, date).order("valid_thru_new_to ASC")
      }
      #self.named_scope :client_by_new_to_date, lambda {|date,client|
      #  {:conditions => ["client_id = ? and valid_thru_new_to <= ?",client, date], :order => "valid_thru_new_to ASC"}
      #}

      self.scope :client_by_new_from_date, lambda {|date,client|
        where("client_id = ? and valid_thru_new_from >= ?", client,date).order("valid_thru_new_from ASC")
      }
      #self.named_scope :client_by_new_from_date, lambda {|date,client|
      #  {:conditions => ["client_id = ? and valid_thru_new_from >= ?", client,date], :order => "valid_thru_new_from ASC"}
      #}
      
      self.scope :client_by_within_new_dates, lambda{|from,to,client|
        where("client_id = ? and (valid_thru_new_from >=? and valid_thru_new_to <= ?)", client,from,to)
      }
      #self.named_scope :client_by_within_new_dates, lambda {|from,to,client|
      #  {:conditions => ["client_id = ? and (valid_thru_new_from >=? and valid_thru_new_to <= ?)", client,from,to]}
      #}
      self.scope :client_by_existence_to_date, lambda{|to,client|
        where("client_id = ? and valid_thru_existence_to <= ?", client,to).order("valid_thru_existence_to ASC")
      }
      #self.named_scope :client_by_existence_to_date, lambda {|to,client|
      #  {:conditions => ["client_id = ? and valid_thru_existence_to <= ?", client,to], :order => "valid_thru_existence_to ASC"}
      #}
      
      self.scope :by_new_date_intervall, lambda{|date,client|
        where("client_id = ? and (valid_thru_new_from <= ? and valid_thru_new_to >= ?)",client,date,date)
      }
      #self.named_scope :by_new_date_intervall, lambda {|date,client|
      #  {:conditions => ["client_id = ? and (valid_thru_new_from <= ? and valid_thru_new_to >= ?)",client,date,date]}
      #}

      self.scope :by_existence_date_intervall, lambda{|date|
        where("client_id = ? and (valid_thru_new_from <= ? and valid_thru_existence_to >= ?)",client,date,date)
      }
      #self.named_scope :by_existence_date_intervall, lambda {|date, client|
      #  {:conditions => ["client_id = ? and (valid_thru_new_from <= ? and valid_thru_existence_to >= ?)",client,date,date]}
      #}
    end

  end

end
