module ActsAsProductForClientEnhanced

  module ProductCollector

    extend self

    class << self

      def deprecate_method(method_name)
        self.send(:alias_method, "old_#{method_name}", method_name)

        define_method method_name do |*args, &block|
          msg = "[TAVEX-MODELS][WARNING]#{method_name} is deprecated and will "
          msg += "be removed in further versions."
          warn msg
          self.send("old_#{method_name}",*args, &block)
        end
      end

    end

    # INTERNAL: Collects products for calculation depending on a
    #           given client, network,category, branch and medium.
    #
    # args - A Hash to define given dependencies.
    #   * client - An instance of Client
    #   * network_id  - A String or Fixnum of a Networks id
    #   * category_id - A Fixnum of a Category's id
    #   * medium      - An instance of Medium
    #   * branch      - An instance of Branch
    #   * user_groups - An array of user groups
    #   * consumption - The consumption value (ht)
    #   * existence   - (Optional) A flag if the collected products are
    #                   existence products
    #   * product_type - valid_for_new? or valid_for_existence?
    #   * date         - The delivery date
    #
    # Returns the selected producst as Array.
    def collect(*args)
      options           = args.extract_options!

      client            = options[:client]
      given_network_id  = options[:network_id]
      category_id       = options[:category_id]
      medium            = options[:medium]
      branch            = options[:branch]
      user_groups       = options[:user_groups]
      consumption       = options[:consumption_value]
      existence         = options.fetch(:existence, false)
      product_type      = options.fetch(:product_type, "valid_for_new?")
      date              = options[:date]

      network_class = "PowerNetwork".constantize
      zone          = :powerzones

      user_group_ids = user_groups.map(&:id)

      if given_network_id.respond_to?(:include?)
        if given_network_id.include?(",")
          network_class    = "GasNetwork".constantize
          zone             = :gaszones
          given_network_id = given_network_id.split(",")
        end
      end

      network_class.class_eval <<-RUBY, __FILE__, __LINE__
        def zones_for_me
          Netzone::Base.by_network_id(:id => self.id,
                                      :network_type => self.class.name.downcase.to_sym)
        end
      RUBY
      netzones = network_class.find(given_network_id).zones_for_me

      # Get the difference of the clients netzones and the network netzones
      # Grab products for netzones first.
      # if there are no netzones we did not need to iterate with any other test.
      return [] if netzones.empty?

      zones_for_client = client.send(zone)#.map(&:id)
      netzones = zones_for_client & netzones

      if existence
        query = lambda do |netzone|
          return netzone.products.includes(:categories, :user_groups).
                          where('categories.branch_id' => branch.id,
                                'visible' => true,
                                'user_groups.id' => user_group_ids)
        end
      else
        if category_id
          query = lambda do |netzone|
            return netzone.products.includes(:categories, :user_groups).
                                    where('categories.id' => category_id,
                                          'visible' => true,
                                          'user_groups.id' => user_group_ids)

          end
        else
          query = lambda do |netzone|
            return netzone.products.includes(:user_groups).
                                    where('visible' => true,
                                          'user_groups.id' => user_group_ids)
          end
        end
      end

      products = netzones.inject([]) do |result, netzone|
        data = query.call(netzone)
        result << data if data.present?
        result
      end

      products.flatten!

      # Grab all which passes the date test
      products = visible_date_and_medium_products(products, :medium => medium, :date => date,
                                                            :type => product_type)

      # Return the products if existence
      return products if existence

      # Grab all which passes the consumption test
      products = products_for_uptake_rates(products,:consumption_value => consumption)

      # return the products
      return products
    end

    private

    # INTERNAL: Filters given products by given user groups
    #
    #  products     - An array of products
    #  opt          - A hash to define
    #   user_groups  - An array of user groups
    #
    # Returns all products that are belonging to a user group given
    # in opt[:user_group]
    def products_for_groups(*args)
      opt = args.extract_options!
      products = args.shift

      products_with_group = []
      products_with_group = products.inject([]) do |result, product|
        if product.user_groups.select{|ug| ug.in?(opt[:user_groups])}.present?
          result << product
        end
        result
      end

      return products_with_group
    end
    deprecate_method :products_for_groups

    # INTERNAL: Filters given products which have belongs to an uptake rate where
    #           a consumption is in the range of the uptake rate
    #
    # products  - An array of products
    # opt       - An hash to define:
    #   consumption value - The consumption value for selecting uptake rates.
    def products_for_uptake_rates(*args)
      opt       = args.extract_options!
      products  = args.shift
      products.select{|prod| not prod.uptake_rates.detect{|up| up.valid_for_consumption?(opt[:consumption_value])}.nil?}
    end

    # INTERNAL: FIlters given products by visibility, date and medium
    #
    # products - An array of products to use the filter on
    # opt - An hash to define:
    #  medium - An instance of Medium
    #  date   - The delivery date
    #  type   - valid_for_new? or valid_for_existence?
    #
    # Returns all products which are passing the visibilty, date and medium tests.
    def visible_date_and_medium_products(*args)
      opt      = args.extract_options!
      products = args.shift
      opt.merge!({:type => "valid_for_new?"}) unless opt.has_key?(:type)
      return products.select do |prod|
        prod.visible? and prod.medium.id == opt[:medium].id and prod.send(opt[:type].to_sym,opt[:date])
      end
    end

  end

end