# -*- encoding : utf-8 -*-
module MediumMethods

  module ClassMethods

    def associations
      belongs_to :medium, :foreign_key => "medium_id", :class_name => "Medium"
    end

  end

  module InstanceMethods

    # medium stuff
    def belongs_to_gas?
      self.medium.is_gas? unless self.medium.nil?
    end

    def belongs_to_power?
      self.medium.is_power? unless self.medium.nil?
    end

  end

end
