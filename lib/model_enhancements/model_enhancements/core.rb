# -*- encoding : utf-8 -*-
module ModelEnhancements
  module Core

    def self.included(base)
      base.send(:extend, ClassMethods)
      base.send(:extend, Associations)
      base.send(:extend, Validations)
      base.send(:extend, Scopes)
    end

    module ClassMethods

      def jsonize!
        json_module = "Json::#{self.name}".constantize
        self.send(:include, json_module)
      end

      def json_for(klass)
        submodule= "Json::#{klass.to_s.camelize}Json::InstanceMethods".constantize
        self.send(:include,submodule)
      end

      # enhancements for client with attached products
      def acts_as_product_for_client_enhanced
        self.send(:include, ActsAsProductForClientEnhanced::InstanceMethods)
      end

      def has_scopes_for_user
        self.extend(Scopes::NamedScopes).user_scopes
      end

      # scopes product
      def acts_as_product_scoped
        self.extend(Scopes::NamedScopes).product_scope
      end

      def acts_as_uptake_rate_scoped
        self.extend(Scopes::NamedScopes).uptake_rate_scope
      end

      def acts_as_identified_scoped
        self.extend(Scopes::NamedScopes).identifier_scopes
      end

      def acts_as_product_date_scoped
        self.extend(Scopes::NamedScopes).product_date_scope
      end

      def acts_as_product_type_filter_scoped
        self.extend(Scopes::NamedScopes).product_type_filters
      end

      def acts_as_client_attached_scoped
        self.extend(Scopes::NamedScopes).client_attached_scopes
      end

      def has_medium_methods
        self.extend(MediumMethods::ClassMethods).associations
        self.send(:include,MediumMethods::InstanceMethods)
      end

      def has_product_date
        self.extend(ProductDates::ClassMethods)
        self.send(:include, ProductDates::InstanceMethods)
      end


      def acts_as_product_xml
        self.send(:include, ProductXml::InstanceMethods)
      end

      def acts_as_product_json
        self.send(:include, Json::ProductJson::InstanceMethods)
      end

      def acts_as_category_json
        self.send(:include, Json::CategoryJson::InstanceMethods)
      end

      def acts_as_campaign_json
        self.send(:include, Json::CampaignJson::InstanceMethods)
      end

      def acts_as_power_network_json
        self.send(:include, Json::PowerNetworkJson::InstanceMethods)
      end

      def acts_as_gas_network_json
        self.send(:include, Json::GasNetworkJson::InstanceMethods)
      end

      def acts_as_client_json
        self.send(:include,Json::ClientJson::InstanceMethods)
      end

      def acts_as_uptake_rate_json
        self.send(:include, Json::UptakeRateJson::InstanceMethods)
      end

      def acts_as_gaszone_json
        self.send(:include, Json::GaszoneJson::InstanceMethods)
      end

      def acts_as_powerzone_json
        self.send(:include, Json::PowerzoneJson::InstanceMethods)
      end

      def acts_as_user_json
        self.send(:include, Json::UserJson::InstanceMethods)
      end


      def has_branch_methods
        self.send(:include, BranchMethods::InstanceMethods)
      end

      def has_product_methods
        self.send(:include, ProductMethods::InstanceMethods)
      end

    end

  end

end
