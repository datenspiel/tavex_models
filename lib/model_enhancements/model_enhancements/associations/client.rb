# -*- encoding : utf-8 -*-
module Associations

  module ClientAssociations

    module ClassMethods

      def associations
        self.with_options :dependent => :destroy do |assoc|
          assoc.has_many :categories, :foreign_key => "client_id", :class_name => "Category"
          assoc.has_many :links, :class_name => "ClientLink", :foreign_key => "client_id"
          assoc.has_many :campaigns, :class_name => "Campaign", :foreign_key => "client_id"
        end
        self.with_options :class_name => "Product", :dependent => :destroy, :foreign_key => "client_id" do |product|
          has_many :products
          has_many :visible_products,:conditions => {:visible => true}
        end
        has_and_belongs_to_many :users, :join_table => "client_users", :foreign_key => "client_id",:association_foreign_key => "user_id", :class_name => "User"
        has_and_belongs_to_many :gaszones, :join_table => "client_gaszones", :foreign_key => "client_id",:association_foreign_key => "zone_id", :class_name => "Gaszone"
        has_and_belongs_to_many :powerzones, :join_table => "client_powerzones", :foreign_key => "client_id", :association_foreign_key => "zone_id", :class_name => "Powerzone"

      end

    end
    
  end

end
