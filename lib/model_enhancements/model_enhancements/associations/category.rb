# -*- encoding : utf-8 -*-
module Associations

  module CategoryAssociations

    module ClassMethods

      def associations
        belongs_to :client, :class_name => "Client", :foreign_key => "client_id"
        belongs_to :medium, :class_name => "Medium", :foreign_key => "medium_id"
        belongs_to :creator, :class_name => "User", :foreign_key => "created_by"
        belongs_to :branch, :class_name => "Branch", :foreign_key => "branch_id"
        has_and_belongs_to_many :products, :join_table => "categories_products", :foreign_key => "category_id", :association_foreign_key => "product_id"
        has_one :quantifier, :class_name=>"Quantifier", :foreign_key=>"category_id"
      end

    end

  end
end
