# -*- encoding : utf-8 -*-
module Associations

  module UserAssociations

    module ClassMethods

      def associations
        
        # usergroups
        has_and_belongs_to_many :groups, :join_table => "users_usergroups", :foreign_key => "user_id", :association_foreign_key => "usergroup_id", :class_name => "UserGroup"
        # creater of user - if master, then himself
        belongs_to :creator, :foreign_key => "created_by", :class_name => "User" 
        has_and_belongs_to_many :clients, :join_table => "client_users", :foreign_key => "user_id",:association_foreign_key => "client_id", :class_name => "Client"

        has_many :created_gaszones, :foreign_key => "created_by", :class_name => "Gaszone"
        has_many :created_powerzones, :foreign_key => "created_by", :class_name => "Powerzone"
        has_many :created_products, :foreign_key => "created_by", :class_name => "Product"
        has_many :created_product_links, :foreign_key => "created_by", :class_name => "ProductLink"
        has_many :created_sales_angles, :foreign_key => "created_by", :class_name => "SalesAngle"
        has_many :created_calc_types, :foreign_key => "created_by", :class_name => "CalcType"
        has_many :created_categories, :foreign_key => "created_by", :class_name => "Category"
        has_many :started_db_synchros, :foreign_key => "started_by", :class_name => "DbSynchro::SynchroWorkflow"
        has_many :campaigns, :foreign_key =>"created_by", :class_name =>"Campaign"

        has_one :preference, :class_name => "Preference", :foreign_key => "user_id"

      end
      
    end
    
  end
  
end
