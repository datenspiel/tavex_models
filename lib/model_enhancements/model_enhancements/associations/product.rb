# -*- encoding : utf-8 -*-
module Associations

  module ProductAssociations

    module ClassMethods

      def associations
        with_options :dependent => :destroy do |assoc|
          assoc.has_many :links, :class_name => "ProductLink", :foreign_key => "product_id"
          assoc.has_many :sales_angles, :class_name => "SalesAngle", :foreign_key => "product_id"
          assoc.has_many :general_conditions, :class_name => "GeneralCondition", :foreign_key => "product_id"
          assoc.has_many :uptake_rates,:class_name => "UptakeRate", :foreign_key => "product_id"
          assoc.has_many :product_modules, :class_name => "ProductModule", :foreign_key => "product_id", :order => "position"
          assoc.has_one :clue, :class_name => "Clue", :foreign_key => "product_id"
          # maybe this has to be refactored into a method with given category. (/Daniel 19.10.2010)
          assoc.has_one :quantifier, :class_name=>"Quantifier", :foreign_key=>"product_id"
        end
        belongs_to :creator, :foreign_key => "created_by", :class_name => "User"
        belongs_to :client, :foreign_key => "client_id", :class_name => "Client"
        belongs_to :calc_type, :class_name => "CalcType",  :foreign_key => "calc_type_id"
        belongs_to :medium, :class_name => "Medium", :foreign_key => "medium_id"

        has_and_belongs_to_many :categories, :join_table => "categories_products", :foreign_key => "product_id", :association_foreign_key => "category_id"

        has_and_belongs_to_many :user_groups, :foreign_key => "product_id", :association_foreign_key => "user_group_id", :class_name =>"UserGroup",
                                :join_table => "product_user_groups"
        has_many :quantifiers, :class_name => "Quantifier", :foreign_key => "product_id", :order => "quantifiers.value"
      end

    end
  end
  
end
