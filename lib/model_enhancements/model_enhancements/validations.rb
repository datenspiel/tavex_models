# -*- encoding : utf-8 -*-
module Validations

  def has_as_product_validations
    self.extend(ProductValidations::ClassMethods).for_product
  end

  def has_as_category_validations
    self.extend(CategoryValidations::ClassMethods).for_category
    self.send(:include, CategoryValidations::InstanceMethods)
  end

  def has_as_client_validations
    self.extend(ClientValidations::ClassMethods).for_client
    self.send(:include, ClientValidations::InstanceMethods)
  end

  def has_as_user_validations
    self.extend(UserValidations::ClassMethods).for_user
    self.send(:include, UserValidations::InstanceMethods)
  end

end
