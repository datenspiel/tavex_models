# -*- encoding : utf-8 -*-
module Json

  module PowerzoneJson

    module InstanceMethods

      def powernetworks_json
        assoc = {:creator_name => self.creator_name, :network_names => attached_networks}
        #assoc.merge!({:networks => nt})
        {self.class.name.underscore.to_sym => self.attributes.merge!(assoc)}
      end

    end

  end

end
