# -*- encoding : utf-8 -*-
module Json

  module OnlineHelpJson

    module InstanceMethods

      
      def as_json(opt={})
        assoc = {}
        assoc.merge!(:site_block => self.site_block.attributes) unless self.site_block.nil?

        {self.class.name.underscore.to_sym => self.attributes.merge!(assoc)}
      end

    end

  end

end
