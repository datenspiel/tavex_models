# -*- encoding : utf-8 -*-
module Json

	module ClientLink

		def as_json(opt={})
			self.attributes.merge!({:creator_name => self.creator_name})
			{self.class.name.underscore.to_sym => self.attributes}
		end

	end


end
