# -*- encoding : utf-8 -*-
module Json

  module CalcTypeJson

    module InstanceMethods

      
      def as_json(opt={})

        methods = {:creator_name => self.creator_name}

        {self.class.name.underscore.to_sym => self.attributes.merge!(methods)}

      end

    end

  end

end
