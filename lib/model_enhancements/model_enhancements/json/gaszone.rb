# -*- encoding : utf-8 -*-
module Json

  module GaszoneJson

    module InstanceMethods

      def gasnetworks_json

        assoc = {:creator_name => self.creator_name, :network_names => self.attached_networks}
        {self.class.name.underscore.to_sym => self.attributes.merge!(assoc)}
      end
#
      #private
#
#
      #def as_json(opt={})
#
      #  assoc = {:networks => self.gasnetworks_json}
#
      #  {self.class.name.underscore.to_sym => self.attributes.merge!(assoc)}
      #end

    end

  end

end
