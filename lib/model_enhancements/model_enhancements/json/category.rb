# -*- encoding : utf-8 -*-
module Json

  module CategoryJson

    module InstanceMethods

     
      def as_json(opt={})
        belongs_to = {}

        belongs_to.merge!({:medium => self.medium.attributes}) unless self.medium.nil?
        belongs_to.merge!({:branch => self.branch.attributes}) unless self.branch.nil?
        #belongs_to.merge!({:quantifier => self.quantifier.attributes}) unless self.quantifier.nil?
        belongs_to.merge!({:creator => self.creator.attributes}) unless self.creator.nil?
        belongs_to.merge!({:client => self.client.attributes}) unless self.client.nil?

        methods = {:quantifier_value => self.try(:quantifier_value),
                   :branch_description => self.try(:branch_description)}


        belongs_to.merge!(methods)

        {self.class.name.underscore.to_sym => self.attributes.merge!(belongs_to)}
      end

    end

  end

end
