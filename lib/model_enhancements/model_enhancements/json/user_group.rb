# -*- encoding : utf-8 -*-
module Json

  module UserGroupJson

    module InstanceMethods


      
      def as_json(opt={})
        assoc = {}
        users_json = []
        self.users.each do |user|
          users_json << user.for_association_json
        end
        assoc.merge!({:users => users_json})
#
        methods = {:locked => self.locked?}
#
        {self.class.name.underscore.to_sym => self.attributes.merge!(assoc.merge!(methods))}
      end
    end
  end
end
