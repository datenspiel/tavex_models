# -*- encoding : utf-8 -*-
module Json

  module ClientJson

    module InstanceMethods

     
      def as_json(opt={})
        assocs = {#:products => self.products.as_json,
                  #:users => self.users.as_json,
                  :gaszones => self.gaszones.as_json,
                  :powerzones => self.powerzones.as_json,
                  :links => self.links.as_json,
                  :categories => self.categories.as_json}
        methods = {
            :logo_url => self.logo_url,
            :only_private => self.only_private?,
            :only_business => self.only_business?,
            :only_power_products => self.only_power_products?,
            :only_heat_products => self.only_heat_products?,
            :only_gas_products => self.only_gas_products?
        }
        assocs.merge!(methods)
        {self.class.name.underscore.to_sym => self.attributes.merge!(assocs)}
      end

    end

  end

end
