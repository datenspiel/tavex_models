# -*- encoding : utf-8 -*-
module Json

  module ProductJson

    module InstanceMethods

      def jsonable(type, opt={})
        self.send(type, opt)
      end

      def product_list_json(opt={})
        calc_methods = {}
        [:brutto_ap,:netto_ap, :brutto_one_month,
         :netto_one_month, :one_month_effective_netto,
         :one_month_effective_brutto,
         :brutto_ap_configured,
         :one_month_effective_brutto_configured,:current_uptake_rate_ap_brutto, :current_uptake_rate_ap_netto,
         :current_uptake_rate_gp_netto,:current_uptake_rate_gp_brutto].each do |meth|
          unless self.send(meth).nil?

            calc_methods.merge!({meth => self.send(meth)})

          end
        end

        methods = {
          :product_modules_count => self.product_modules.size,
          :valid_new_to_german => self.valid_new_to_german,
          :valid_thru_new_from_german => self.valid_thru_new_from_german,
          :valid_existence_to_german => self.valid_existence_to_german,
          :has_default_options => self.has_default_options?,
        }

        assoc={
          :product_modules => self.product_modules.as_json
        }.merge!(methods).merge!(calc_methods)

        {self.class.name.underscore.to_sym => self.attributes.merge!(assoc)}
      end

      def json_for_calculation(opt={})
        json = as_json(:include => [:clue, :client, :creator, :calc_type])
        [:brutto_ap,:netto_ap, :brutto_one_month,
         :netto_one_month, :one_month_effective_netto,
         :one_month_effective_brutto,
         :brutto_ap_configured,
         :one_month_effective_brutto_configured,:current_uptake_rate_ap_brutto, :current_uptake_rate_ap_netto,
         :current_uptake_rate_gp_netto,:current_uptake_rate_gp_brutto].each do |meth|
          unless self.send(meth).nil?
            json[self.class.name.underscore][meth.to_s] = self.send(meth)
          end
        end
        json[self.class.name.underscore]["product_modules_count"] = self.product_modules.size
        json[self.class.name.underscore]["belongs_to_power"] = self.belongs_to_power?
        json[self.class.name.underscore]["belongs_to_gas"]   = self.belongs_to_gas?
        json[self.class.name.underscore]["valid_new_to_german"] = self.valid_new_to_german
        json[self.class.name.underscore]["valid_thru_new_from_german"] = self.valid_thru_new_from_german
        json[self.class.name.underscore]["valid_existence_to_german"]  = self.valid_existence_to_german
        json[self.class.name.underscore]["has_default_options"] = self.has_default_options?
        json[self.class.name.underscore]["has_categories"] = self.has_categories?
        json[self.class.name.underscore]["is_gas"] = self.is_gas_product?

        if self.respond_to?(:product_info)
          json[self.class.name.underscore]["has_product_infos"] = (not self.product_info.nil?)
        end

        if self.medium.present?
          json[self.class.name.underscore]["medium"] = self.medium.json_attributes
        end

        [ :links,
          :sales_angles,
          :general_conditions,
          :uptake_rates,
          :quantifiers
          ].each do |association|
            json[self.class.name.underscore][association.to_s] = self.send(association).as_json
          end
        json[self.class.name.underscore]["product_modules"] = self.product_modules.select{|mod| not mod.options.empty?}.as_json
        json[self.class.name.underscore]["categories"] = self.attached_categories_as_json
        # not really necessary for frontend?
        if opt.has_key?(:include)
          if(opt[:include].include?(:groups))
            json[self.class.name.underscore]["user_groups"] = self.user_groups.collect{|n| n.for_association_json}
          end
          if(opt[:include].include?(:zones))
            # not really necessary for frontend?
            json[self.class.name.underscore]["zones"] = self.zones.as_json
          end
        end
        # merge assocs if product responds_to :compare => existence products
        if self.respond_to?(:lucency_networks)
          json[self.class.name.underscore]["has_lucency_networks"] = (not self.lucency_networks.empty?)
        end
        json
      end
      alias_method :single_json_output, :json_for_calculation

      def attached_categories_as_json
        cats = []
        self.categories.each do |category|
          value = category.quantifier_value_with_product(self)
          json = { category.class.name.underscore.to_sym => category.attributes.merge!(:quantifier_value => value).merge!(:branch_description => category.try(:branch_description))}
          cats << json
        end
        return cats
      end

      def as_json(opt={})
        json = super
        json[self.class.name.underscore]["creator_name"] = self.creator_name
        return json
      end

    end

  end

end
