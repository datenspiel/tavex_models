# -*- encoding : utf-8 -*-
module Json

  module CampaignJson

    module InstanceMethods

      def as_json(opt={})
        assoc = {}

        assoc.merge!({:medium => self.medium.attributes}) unless self.medium.nil?
        assoc.merge!({:branch => self.branch.attributes}) unless self.branch.nil?
        assoc.merge!({:client => self.client.attributes}) unless self.client.nil?
        assoc.merge!({:creator => self.creator.attributes}) unless self.creator.nil?

        # join german dates
        assoc.merge!({:valid_thru_german => self.valid_thru_german,
                      :valid_from_german => self.valid_from_german})

        assoc.merge!({:powerzones => self.powerzones.as_json,
                       :gaszones => self.gaszones.as_json
                      })

        {self.class.name.underscore.to_sym => self.attributes.merge!(assoc)}
      end

    end

  end

end
