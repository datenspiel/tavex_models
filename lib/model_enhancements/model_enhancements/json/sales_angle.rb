# -*- encoding : utf-8 -*-
module Json

  module SalesAngleJson

    module InstanceMethods

      
      def as_json(options={})
        attributes = {
            :short_description => self.short_description,
            :description => self.description,
            :id => self.id,
            :creator_name => self.creator_name,
            :created_at => self.created_at
          }
        belongs_to = {}
        belongs_to.merge!({:icon => self.icon.attributes}) unless self.icon.nil?
        {self.class.name.underscore.to_sym => attributes.merge!(belongs_to)}
        #self.attributes.merge!(assoc)
      end
    end
      
  end
end
