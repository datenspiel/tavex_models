# -*- encoding : utf-8 -*-
module Json

  module UptakeRateJson

    module InstanceMethods

      
      def as_json(opt={})
        belongs_to = {}

        belongs_to.merge!({:product => self.product.attributes}) unless self.product.nil?
        belongs_to.merge!({:creator => self.creator.attributes}) unless self.creator.nil?

        {self.class.name.underscore.to_sym => self.attributes.merge!(belongs_to)}
      end

    end

  end


end
