# -*- encoding : utf-8 -*-
module Json

  module ProductLinkJson

    module InstanceMethods

      
      def as_json(opt={})
        assoc = {}
        assoc.merge!({:creator_name => self.creator_name})
        assoc.merge!({:product => self.product.attributes}) unless self.product.nil?
        {self.class.name.underscore.to_sym => self.attributes.merge!(assoc)}
      end

    end
    
  end

end
