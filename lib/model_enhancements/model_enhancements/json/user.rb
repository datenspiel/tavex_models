# -*- encoding : utf-8 -*-
module Json
  module UserJson

    module InstanceMethods

      def methods_json
        {
          :creator_name => self.creator_name,
          :group_names => group_names,
          :client_names => client_names
        }
      end

      def groups_json
        ug_json = []
        self.groups.each do |group|
          ug_json << {group.class.name.underscore.to_sym => group.attributes}
        end
      end

      def clients_json
        clients_js = []
          self.clients.each do |client|
          clients_js << {:client => client.attributes.merge!({:logo_url => client.logo_url}).merge!(:links => client.links.as_json)}
        end
      end

      def single_json_output

        preference = self.preference.nil? ? {} : {:preference => self.preference.attributes}

        additionals = {:clients => clients_json,:groups => groups_json}.merge!(preference)
        {self.class.name.underscore.to_sym => self.attributes.merge!(methods_json).merge!(additionals)}
      end

      
      def as_json(opt={})
        {self.class.name.underscore.to_sym => self.attributes.merge!(methods_json)}
      end

    end

  end
end
