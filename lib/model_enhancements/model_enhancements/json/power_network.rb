# -*- encoding : utf-8 -*-
module Json

  module PowerNetworkJson

    module InstanceMethods

      def list_as_json
        list_attributes = {
          :name => self.name,
          :network_id => self.network_id,
          :id => self.network_id,
          :name => self.name
        }

        {self.class.name.underscore.to_sym => list_attributes}
      end

      def as_json(opt={})
        assoc = {}

        assoc.merge!({:power_network_carrier => self.power_network_carrier.attributes}) unless self.power_network_carrier.nil?
        assoc.merge!({:power_cities => self.power_cities.as_json,
                  :powerzones => self.powerzones.as_json,
                  :powerzone_ids => self.powerzone_ids})
        assoc.merge!({:carrier => self.carrier.attributes}) if self.carrier
        {self.class.name.underscore.to_sym => self.attributes.merge!(assoc)}
      end

    end

  end

end
