# -*- encoding : utf-8 -*-
module Json

	module GeneralCondition

		def as_json(opt={})
			attributes = {
        :short_description => self.short_description,
        :description => self.description,
        :id => self.id,
        :creator_name => self.creator_name,
        :created_at => self.created_at
      }
    	belongs_to = {}
    	belongs_to.merge!({:icon => self.icon.attributes}) unless self.icon.nil?
			{self.class.name.underscore.to_sym => attributes.merge!(belongs_to)}
		end

	end

end
