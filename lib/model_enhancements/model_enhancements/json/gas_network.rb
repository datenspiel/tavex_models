# -*- encoding : utf-8 -*-
module Json

  module GasNetworkJson

    module InstanceMethods

      def list_as_json()
        list_attributes = {
          :id => self.id,
          :network_id => self.network_id,
          :name => self.name,
          :network_domain => self.network_domain,
          :subnetwork => self.subnetwork
        }
        {self.class.name.underscore.to_sym => list_attributes}
      end

      def as_json(opt={})
        assoc = {}

        assoc.merge!({:gaszones => self.gaszones.as_json,
                      :cities => self.cities.as_json})
        methods = {
                   :gaszone_ids => self.gaszone_ids}
        carriers = []
        self.gas_network_carriers.each do |gc|
          carriers << {:gas_network_carrier => gc.attributes}
        end
        methods.merge!(:gas_network_carriers => carriers)
        assoc.merge!(methods)
        assoc.merge!({:composite_id => composite_id})
        if self.network_carrier.present?
          assoc.merge!({:gas_network_carrier => self.network_carrier.attributes})
        end
        {self.class.name.underscore.to_sym => self.attributes.merge!(assoc)}
      end

    end

  end

end
