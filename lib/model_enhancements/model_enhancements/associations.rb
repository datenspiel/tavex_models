# -*- encoding : utf-8 -*-
module Associations

  def has_associations_as_product
    self.extend(ProductAssociations::ClassMethods).associations
  end

  def has_associations_as_category
    self.extend(CategoryAssociations::ClassMethods).associations
  end

  def has_associations_as_client
    self.extend(ClientAssociations::ClassMethods).associations
  end

  def has_associations_as_user
    self.extend(UserAssociations::ClassMethods).associations
  end

end
