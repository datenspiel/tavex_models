# -*- encoding : utf-8 -*-
module ProductMethods

  module InstanceMethods

    #@author Jaqueline Wilde
    #@return [boolean] true if client has only power_products
    def only_power_products?
      power=AveConfig::Config::configuration.power
      other_than_power_found = false
      self.products.each do |product|
        unless product.medium.nil?
          if product.medium.id != power
            other_than_power_found = true
            break
          end
        end
      end
      return !other_than_power_found
    end

    #@author Jaqueline Wilde
    #@return [boolean] true if client has only gas_products
    def only_gas_products?
      gas=AveConfig::Config::configuration.gas
      other_than_gas_found = false
      self.products.each do |product|
        unless product.medium.nil?
          if product.medium.id != gas
            other_than_gas_found = true
            break
          end
        end
      end
      return !other_than_gas_found
    end

    #@author Jaqueline Wilde
    #@return [boolean] true if client has only heat_products
    def only_heat_products?
      #heat_products=[]
      other_than_heat_found = false
      heat=AveConfig::Config::configuration.heat
      #self.products.each do |product|
      #  unless product.medium.nil?
      #    if product.medium.id == heat
      #      heat_products<<product
      #    end
      #end
      self.products.each do |product|
        unless product.medium.nil?
          if product.medium.id != heat
            other_than_heat_found = true
            break
          end
        end
      end
      #if self.products==heat_products
      #  return true
      #else
      #  return false
      return !other_than_heat_found
    end

  end


end
