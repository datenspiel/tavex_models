# -*- encoding : utf-8 -*-
module ProductDates

  module ClassMethods


  end

  module InstanceMethods

    #@author Jaqueline Wilde
    #return false if dates for valid_thru_new_to and valid_thru_new_from or valid_thru_existence_to and valid_thru_new_from are not valid
    #@return [boolean] false, if valid_thru_new_from not earlier than valid_thru_new_to or valid_thru_new_from not earlier than valid_thru_existence_to
    def valid_dates?
      unless(self.valid_thru_new_from==nil || self.valid_thru_new_to==nil)
        if(self.valid_thru_new_from>self.valid_thru_new_to)
          errors.add_to_base("Daten nicht zulässig!")
          #false
        end
      end
      unless(self.valid_thru_new_from==nil || self.valid_thru_existence_to==nil)
        if(self.valid_thru_new_from>self.valid_thru_existence_to)
          errors.add_to_base("Daten nicht zulässig!")
          #false
        end
      end
    end

    # Date stuff
    def valid_thru_new_from_german
      valid_thru_new_from.to_ymd unless valid_thru_new_from.nil?
    end

    def valid_new_to_german
      valid_thru_new_to.to_ymd unless valid_thru_new_to.nil?
    end

    def valid_existence_to_german
      valid_thru_existence_to.to_ymd unless valid_thru_existence_to.nil?
    end

    # this are fake setters for rubyamf
    def valid_thru_new_from_german=(p);end
    def valid_new_to_german=(p);end

    def valid_for_new?(date)
      date_i = integerize_date.call(date)

      valid = false
      unless(self.valid_thru_new_from==nil || self.valid_thru_new_to==nil)
        valid_thru_new_from_i = integerize_date.call(self.valid_thru_new_from)
        valid_thru_new_to_i   = integerize_date.call(self.valid_thru_new_to)
        valid = valid_thru_new_from_i <= date_i && valid_thru_new_to_i >= date_i
      end
      return valid
    end

    def valid_for_existence?(date)
      date_i  = integerize_date.call(date)
      valid   = false
      if valid_thru_existence_to.present?
        ext_valid = integerize_date.call(valid_thru_existence_to)
        valid = ext_valid >= date_i
      end
      return valid
    end

    private

    def integerize_date
      lambda do |date|
        return date.beginning_of_day.to_i
      end
    end

  end

end
