# -*- encoding : utf-8 -*-
# Provides instance methods for an existing product like comparing to other products.
module ActsAsExistingProduct

  attr_accessor :status, :diff_brutto, :diff_brutto_month

  SUMMARY_CHEAP = 0 # Bestandsprodukt ist günstiger
  SUMMARY_EXPENSIVE = 1 #Bestandsprodukt ist teurer
  SUMMARY_EQUALS = 2 # Bestandsprodukt weisst gleichen Preis auf

  # existing product is cheaper
  # @author Daniel Schmidt
  # @return [Fixnum] existing product is cheaper than the compared one
  def cheaper
    SUMMARY_CHEAP
  end

  # existing product is more expansiv
  # @author Daniel Schmidt
  # @return [Fixnum] existing product is more expansiv than the compared one
  def more_expensive
    SUMMARY_EXPENSIVE
  end

  # existing products isn't cheaper neither more expansive
  # @author Daniel Schmidt
  # @return [Fixnum]
  def equals
      SUMMARY_EQUALS
  end

  # compares self with given products
  # @author Daniel Schmidt
  # @return [Hash] - wenn product in einer kategorie abweicht => gibt array von hashs zurück:
  # @param [Array] products Products to compare with.
  # @example return values
  #   [{:bestand => self, :summary => self.cheaper}]
  def compare(products)
    compared = []
    products.each do |prod|
      # alle ergebnisse mit self.ergebnissen vergleichen
      # abweichung nach oben => more_expensive
      # abweichung nach unten => cheaper
      # keine abweichung => equals
      k = {}
      #count = 0
      if prod.brutto_ap > self.brutto_ap
        k[:status] = status(self.cheaper)
      elsif prod.brutto_ap < self.brutto_ap
        k[:status] = status(self.more_expensive)
      else
        k[:status] = status(self.equals)
      end

      k[:diff_brutto] = (self.brutto_ap - prod.brutto_ap)
      k[:diff_brutto_monthly] = ((self.brutto_ap/self.client.monthly_divider)-(prod.brutto_ap/client.monthly_divider))

      #prod.prices.each do |result|
      #  if result > self.prices[count]
      #    k[:summary] = self.cheaper
      #    @status ="cheaper"
      #    p "cheaper" + @status
      #  elsif result < self.prices[count]
      #    k[:summary] = self.more_expensive
      #    @status="expensive"
      #  else
      #    k[:summary] = self.equals
      #
      #    @status="equals"
      #    p "equals" + @status
      #  end
      #  count +=1
      #end
      #k.rehash
      compared << k
    end
    
    compared
  end

  private

  def status(value)
   case value
      when self.cheaper then "cheaper"
      when self.more_expensive then "expensive"
      when self.equals then "equals"
   end
  end
end
