# -*- encoding : utf-8 -*-
module BranchMethods

  module InstanceMethods

    #@author Jaqueline Wilde
    #@return [boolean] true if the branch of all categories of client is private
    def only_private?
      branches=[]
      private_branches=[]
      has_private = false
      self.categories.each do |category|
        branches<<category.branch
        if category.branch.is_private?
          private_branches<<category.branch
        end
      end
      if branches==private_branches
        has_private = true
      end
      has_private
    end

    #@author Jaqueline Wilde
    #@return [boolean] true if the branch of all categories of client is business
    def only_business?
      branches=[]
      business_branches=[]
      self.categories.each do |category|
        branches<<category.branch
        if category.branch.is_business?
          business_branches<<category.branch
        end
      end
      if branches==business_branches
        return true
      else
        return false
      end
    end

  end
  
end
