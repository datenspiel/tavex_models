# -*- encoding : utf-8 -*-
module ActsAsProductForClientEnhanced

  module InstanceMethods

    # opt may something like this:
    # {:consumption_value => 6000, :medium => [Power/Strom], :date => Time.now, :branch_identifier => "Power",
    #  :network_id => 193267}
    def select_products_for_productlist(opt={})
      if opt.empty?
        []
      else
        medium = opt[:medium]
        raise AveErrors::AveNotAMediumError, "not a medium" unless medium.is_a?(Medium)
        # collect only visible and medium and date proper products
        #tmp_products = collect_visible_date_and_medium_products(:medium => medium, :date => opt[:date])
        # filter all products which belongs to the usergroups of current user
        #tmp_products = collect_products_for_groups(:products => tmp_products, :user_groups => opt[:user_groups])
        # collect all products which categories belongs to the given branch
        #tmp_products = tmp_products.select{|prod| not prod.categories.detect{|cat| cat.send("is_#{opt[:branch_identifier]}?")}.nil?}
        # collect all products whose zones contains the network
        #tmp_products = collect_products_for_network(:network_id => opt[:network_id], :products => tmp_products)
        # collect all products which consumption value passes the uptake_rates
        #tmp_products = collect_products_for_uptake_rates(:products => tmp_products, :consumption_value => opt[:consumption_value])
        
        tmp_products = ProductCollector.collect(opt.merge!(:client => self))
        return tmp_products
      end
    end

    def select_existence_products_for_productlist(opt={})
      return [] if opt.empty?
      tmp_products = ProductCollector.collect(opt.merge!(:client => self, :existence => true))
      return tmp_products
    end


    # Handles select_new_business_products and select_existence_products methods.
    def method_missing(method_sym, *arguments, &block)
      match = method_sym.to_s =~/select_/
      if match
        # split method name at '_'
        method_splitted = method_sym.to_s.split("_")
        method_splitted.slice!(0)
        #join splitted[1] and splitted [2]
        deeper_method_name = method_splitted.join("_")
        deeper_match_existence = deeper_method_name =~/existence_business_products/
        deeper_match_new = deeper_method_name =~/new_business_products/
        if deeper_match_new or deeper_match_existence
          tmp_products = []
          opt = arguments.pop
          if opt.nil?
            return tmp_products
          elsif opt.empty?
            return tmp_products
          else
            medium = opt[:medium]
            raise AveErrors::AveNotAMediumError, "not a medium" unless medium.is_a?(Medium)
            type = deeper_match_new ? "valid_for_new?" : "valid_for_existence?"

            if deeper_match_existence
              opt.merge!({:products => tmp_products})
              opt.merge!({:existence => true}) 
            end
            collected_products = ProductCollector.collect(opt.merge!(:client => self,
                                                                    :product_type => type))

            #collected_products = collect_products(opt)
            if deeper_match_existence
              collected_products.each{|prod| prod.singleton.send(:include, ::ActsAsExistingProduct)}
            end
            return collected_products
          end
        else
          super
        end
      else
        super
      end
    end

   
  end

end
