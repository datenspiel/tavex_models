# -*- encoding : utf-8 -*-
module Validations

  module UserValidations

    module ClassMethods

      def for_user

         validates_uniqueness_of :login, :message => " must be unique", :on => :create
         #validates_presence_of :creator, :on => :create,:message => "can't be blank"
         validates_presence_of :name, :on => :create
         validates_presence_of :surename, :on => :create
         validates_uniqueness_of :email
         #validates_presence_of :groups


      end

    end

    module InstanceMethods
      #@author Jaqueline Wilde
      #returns false if login is "master" or "Master" or "MASTER"
      #@return [boolean] false, if login is "master"
      def check_login
        if(self.login.casecmp("master")==true)
          errors.add_to_base("Login darf nicht 'master' sein.")
          false
        end
      end

      def check_lengths
        #puts "AVECONFIG" * 20
        #puts AveConfig::Config::configuration.users_name_length
        unless self.login.nil? and self.login.downcase.eql?("master")
          unless self.name.nil?
            return false if (self.name.length>=AveConfig::Config::configuration.users_name_length)
          end
          unless self.surename.nil?
           return false if (self.surename.length>=AveConfig::Config::configuration.users_surename_length)
          end
          unless self.login.nil?
            return false if (self.login.length>=AveConfig::Config::configuration.users_login_length)
          end
          unless self.email.nil?
            return false if (self.email.length>=AveConfig::Config::configuration.users_email_length)
          end
          #if (self.password.length>=AveConfig::Config::configuration.users_password_length)
          #  return false
          #end
        end
      end


      def check_if_master
        if(self.login=="master")
          errors.add_to_base("master darf nicht gelöscht werden.")
          false
        end
      end

    end

  end

end
