# -*- encoding : utf-8 -*-
module Validations

  module CategoryValidations

    module ClassMethods

      def for_category
        validates_presence_of :identifier, :message => "Bezeichnung darf nicht fehlen"
        validates_presence_of :branch, :on => :create
        validates_presence_of :medium, :on => :create  
      end

    end

    module InstanceMethods
      #@author Jaqueline Wilde
      #checks lenghts of identifier
      #@return [string] identifier,cut down to allowed length
      def check_length_of_identifier
        allowed_length=AveConfig::Config::configuration.category_identifier_length
        length=self.identifier.length
        if length > allowed_length
          self.identifier.slice!(allowed_length..(length-1))
        end
      end
    end

  end

end
