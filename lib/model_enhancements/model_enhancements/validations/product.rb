# -*- encoding : utf-8 -*-
module Validations

  module ProductValidations

    module ClassMethods

      def for_product
        #Product here
        validates_presence_of :identifier#, :message => "Bezeichner darf nicht fehlen"
        validates_presence_of :info_short#, :message => "Kurzbezeichnung darf nicht fehlen"
        #validates_presence_of :minimum_price, :message => "Minimumpreis darf nicht fehlen"
        validates_format_of :minimum_price, :with => /(^(\d+)(\.)?(\d+)?)|(^(\d+)?(\.)(\d+))/
        validates_presence_of :client#, :message => "Mandant darf nicht fehlen!"
        validates_presence_of :medium#, :message => "medium darf nicht fehlen"

        validate :valid_dates?
      end

    end

  end

end
