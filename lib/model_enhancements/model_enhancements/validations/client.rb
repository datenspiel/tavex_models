# -*- encoding : utf-8 -*-
module Validations

  module ClientValidations

    module ClassMethods

      def for_client
        validates_presence_of :identifier, :message => "Mandantenbezeichner darf nicht fehlen!"
        validates_uniqueness_of :identifier, :message => "must be unique"
        before_save :check_length_of_identifier
        validates_format_of :exchange_ratio, :with => /(^(\d+)(\.)?(\d+)?)|(^(\d+)?(\.)(\d+))/, :allow_nil=>true
      end

    end

    module InstanceMethods

      # @author Jaqueline Wilde
      # checks lenghts of identifier
      # @return [string] identifier,cut down to allowed length
      def check_length_of_identifier
        allowed_length=AveConfig::Config::configuration.client_identifier_length
        length=self.identifier.length
        if length > allowed_length
          self.identifier.slice!(allowed_length..(length-1))
        end
      end

    end

  end

end
