# -*- encoding : utf-8 -*-
module ProductXml

  module InstanceMethods

    def to_xml(options={})
      options[:indent] ||= 2
      xml = options[:builder] ||= Builder::XmlMarkup.new(:indent => options[:indent])
      xml.instruct! unless options[:skip_instruct]
      build_xml(xml)
    end

    private
    def build_xml(xml)
      xml.Product{
        xml.tag!("valid_thru_existence_to".camelize, valid_thru_existence_to.to_as3_date)
        xml.tag!("valid_thru_new_from".camelize, valid_thru_new_from.to_as3_date)
        xml.tag!("valid_thru_new_to".camelize, valid_thru_new_to.to_as3_date)
        xml.tag!("created_at".camelize, created_at)
        xml.tag!("updated_at".camelize, updated_at)
        xml.tag!("identifier".camelize, identifier)
        xml.tag!("id".camelize, id, :type => :integer)
        xml.tag!("rate_key".camelize, rate_key)
        xml.tag!("medium_id".camelize, medium_id)
        xml.tag!("info_short".camelize, info_short)
        xml.tag!("info_long".camelize, info_long)
        xml.tag!("minimum_price".camelize, minimum_price, :type=> :float)
        xml.tag!("cumulatively".camelize, cumulatively, :type => :boolean)
        xml.tag!("mp_comment".camelize, mp_comment)
        xml.tag!("type".camelize,type)

        # prices !
        xml.tag!("brutto_ap".camelize, brutto_ap) unless brutto_ap.nil?
        xml.tag!("netto_ap".camelize, netto_ap) unless netto_ap.nil?
        xml.tag!("brutto_one_month".camelize, brutto_one_month) unless brutto_one_month.nil?
        xml.tag!("netto_one_month".camelize, netto_one_month) unless netto_one_month.nil?
        xml.tag!("one_month_effective_netto".camelize, one_month_effective_netto) unless one_month_effective_netto.nil?
        xml.tag!("one_month_effective_brutto".camelize, one_month_effective_brutto) unless one_month_effective_brutto.nil?

        unless self.client.nil?
          xml.Client do
            xml.tag!("identifier".camelize, self.client.identifier)
          end
        end

        unless self.product_modules.empty?
          xml.tag!("product_modules_count".camelize,self.product_modules.size)
          xml.ProductModules do
            self.product_modules.each do |mod|
              xml.ProductModule{
                xml.tag!("name".camelize, mod.name)
              }
            end
          end
        else
          xml.tag!("product_modules_count".camelize,0)
        end

        unless self.uptake_rates.empty?
          xml.UptakeRates do
            self.uptake_rates.each do |rate|
              xml.UptakeRate{
                xml.tag!("id".camelize, rate.id)
                xml.tag!("kilowatt_hour_from".camelize, rate.kilowatt_hour_from)
                xml.tag!("kilowatt_hour_to".camelize, rate.kilowatt_hour_to)
                xml.tag!("gp".camelize, rate.gp)
                xml.tag!("ht".camelize, rate.ht)
                xml.tag!("nt".camelize, rate.nt)
              }
            end
          end
        end

      }

    end

  end

end
