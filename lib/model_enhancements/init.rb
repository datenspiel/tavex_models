# -*- encoding : utf-8 -*-
# load all enhancements
Dir[File.expand_path(File.join(File.dirname(__FILE__),'model_enhancements','*.rb'))].each {|f| require f}
Dir[File.expand_path(File.join(File.dirname(__FILE__),'model_enhancements','validations','*.rb'))].each {|f| require f}
Dir[File.expand_path(File.join(File.dirname(__FILE__),'model_enhancements','associations','*.rb'))].each {|f| require f}
Dir[File.expand_path(File.join(File.dirname(__FILE__),'model_enhancements','json','*.rb'))].each {|f| require f}
Dir[File.expand_path(File.join(File.dirname(__FILE__),'model_enhancements','scopes','*.rb'))].each {|f| require f}

ActiveRecord::Base.send(:include, ModelEnhancements::Core)
