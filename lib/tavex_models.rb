# -*- encoding : utf-8 -*-
# Require ActiveRecord enhancements first.
require File.expand_path(File.join(File.dirname(__FILE__), 'model_enhancements', 'init.rb'))
# Require all models
#Dir[File.expand_path(File.join(File.dirname(__FILE__), 'models', '*.rb'))].each{|model| require model}

require File.expand_path(File.join(File.dirname(__FILE__), 'models', 'base.rb'))
require File.expand_path(File.join(File.dirname(__FILE__), 'models', 'branch.rb'))
require File.expand_path(File.join(File.dirname(__FILE__), 'models', 'client.rb'))
require File.expand_path(File.join(File.dirname(__FILE__), 'models', 'medium.rb'))
